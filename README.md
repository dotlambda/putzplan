# SSD1306 I2C/SPI OLED LCD Example

To run this example connect the SSD1306 OLED LCD and configure protocol, display size and pins in main.c file.

## Build environment

The build environment is originally from https://github.com/bschwind/esp-build.

Build the Docker image using

    docker build -t esp-open-rtos .

and open a shell with the build and flashing tools using

    docker run -it --privileged -e "ESPBAUD=921600" -v /dev/bus/usb:/dev/bus/usb -v $PWD:/home/esp/project esp-open-rtos bash

Then you can compile the program using `make` and flash it with `make flash`.
